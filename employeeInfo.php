<?php
/**
 * Created by PhpStorm.
 * User: Jay
 * Date: 2016-04-08
 * Time: 9:35 PM
 *
 * 2. Build PHP script to access Database:

The script will use the MySQLI class to connect to the database.

Your PHP file will be named employeeInfo.php. This page should have a centered <div> with id of "empData" to hold content (700px wide).
 * The background of your page should be seafoam green and the background of your container <div> should be white.
 * Please use a font size of 14px.

Given this url with parameter id,  employeeInfo.php?id=10001, extract the id using $_GET['id']

On your page you will display (given whatever ID you pass in via the url):

Employee first name, last name, gender, date of birth, hire-date, and their current title.
Display the employees Department name.
Finally in a table you will list their most recent 4 salary records with salary, start date and end date for each record.

To RECAP:

Given the url: http://localhost/employeeInfo.php?id=10001
 *
 *
 */

    // Set the id to a variable if a GET request is sent
    if(isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    // Make conection to the database
    @ $db = new mysqli('localhost', 'root', 'root', 'employees');

    // Gives the user an error if the conection to the database could not be made
    if (mysqli_connect_errno()) {
        echo 'Error: Could not connect to database. Please try again later.';
        exit;
    }


    // Build the query to find the employee and store it to a variable
    $empQuery = "SELECT * FROM employees where emp_no=".$id;
    // Store the result of the query into a variable
    $employeeResult = $db -> query($empQuery);
    // Get the number of rows or results returned from the result object
    $numResults = $employeeResult->num_rows;
    $isResults = true;

    if($numResults == 0) {
        $isResults = false;
    } else {

        $empRow = $employeeResult->fetch_assoc();

        $firstName = $row['first_name'];
        $lastName = $row['last_name'];
        $empNo = $row['emp_no'];
        $gender = $row['gender'];
        $birthDate = $row['birth_date'];
        $hireDate = $row['hire_date'];

        // Query to find the job titles for the emp id
        $jobTitleQuery = "SELECT title FROM title WHERE emp_no=".$id;
        // Query to find the 4 salaries for the employee with the emp id in the GET request
        $salaryQuery = "SELECT * FROM salaries WHERE emp_no=".$id." LIMIT 4";
        // Results of the title query
        $titleResult = $db->query($jobTitleQuery);
        // Results of the salary query
        $salaryResult = $db->query($salaryQuery);
        // Set a row for the salary request
        $salRow = $salaryResult->fetch_assoc();
        // Set a row for the title request
        $titleRow = $titleResult->fetch_assoc();


    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="custom.css" rel="stylesheet">
</head>
<body>
    <div id="empData">



    </div>
</body>
</html>
